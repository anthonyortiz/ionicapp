import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConexionService {

  constructor(private http: HttpClient) { }

  getDetail(name): Observable<any> {
    return this.http.get('https://api.github.com/search/users?q=' + name);
  }
}
